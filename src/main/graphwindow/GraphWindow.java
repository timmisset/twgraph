package main.graphwindow;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import org.apache.batik.swing.svg.JSVGComponent;
import org.jetbrains.annotations.NotNull;
import util.Util;

import java.io.IOException;
import java.io.InputStream;

public class GraphWindow implements ToolWindowFactory {

    private static InputStream ttlfile = util.Util.getSampleFile("university.ttl");

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        try {
            toolWindow.getContentManager().addContent(getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }

//        TTL2Graph ttl2Graph = new TTL2Graph(Util.getSampleFile("university.ttl"));
//        System.out.println(ttl2Graph.toGraphString("http://xmlns.com/foaf/0.1/Person"));

    }

    private Content getContent() throws IOException {
        JSVGComponent component = Graph2SVG.toJSVG(graphString);
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        return contentFactory.createContent(component, "My Toolwindow", false);
    }

    private static String graphString = "digraph finite_state_machine {\n" +
            "size=\"8,5\"\n" +
            "node [shape = circle];\n" +
            "foaf_Person -> _Student [ label = \"Subclass\"];\n" +
            "_Student -> _UndergraduateStudent [ label = \"Subclass\"];\n" +
            "_Student -> _GraduateStudent [ label = \"Subclass\"];\n" +
            "foaf_Person -> _Researcher [ label = \"Subclass\"];\n" +
            "_Researcher -> _Professor [ label = \"Subclass\"];\n" +
            "_Researcher -> _PostDoc [ label = \"Subclass\"];\n" +
            "_Researcher -> _PhDStudent [ label = \"Subclass\"];\n" +
            "foaf_Person -> _Teacher [ label = \"Subclass\"];\n" +
            "_Teacher -> _Professor [ label = \"Subclass\"];\n" +
            "_Teacher -> _ExternalTeacher [ label = \"Subclass\"];\n" +
            "}\n";


}
