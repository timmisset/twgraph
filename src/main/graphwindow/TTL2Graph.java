package main.graphwindow;

import java.io.InputStream;
import java.util.*;

import org.apache.jena.graph.Graph;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

public class TTL2Graph {

    private InputStream ttlData;
    private Model model;
    private Map<String, String> prefixMap;
    public TTL2Graph(InputStream ttlData) {
        model = ModelFactory.createDefaultModel();
        model.read(ttlData, "", "Turtle");
        prefixMap = model.getNsPrefixMap();
        Set<String> keys = new HashSet<>();
        keys.addAll(prefixMap.keySet());
        for(String key : keys) {
            prefixMap.put(prefixMap.get(key), key);
        }
    }

    public Graph getGraph() {
        Graph graph = model.getGraph();
        return graph;
    }

    public void explore(String uri) {
        List<Resource> classes = getClasses();
        for(Resource _class : classes) {
            System.out.println(_class);
        }
    }
    public List<Resource> explore() {
        return getTypes();
    }

    public List<Resource> getTypes() {
        return iteratorToList(model.listSubjectsWithProperty(RDF.type));
    }
    public List<Resource> getClasses() {
        List<Resource> classes = new ArrayList<>();
        classes.addAll(iteratorToList(model.listSubjectsWithProperty(RDF.type)));
        classes.addAll(iteratorToList(model.listSubjectsWithProperty(RDFS.subClassOf)));

        return classes;
    }
    private List<Resource> getClasses(List<Resource> collected, Resource resource) {
        List<Resource> subClasses = iteratorToList(
                resource == null ? model.listSubjectsWithProperty(RDF.type) : model.listSubjectsWithProperty(RDFS.subClassOf, resource));

        for(Resource _class : subClasses) {
            getClasses(collected, _class);
            collected.add(_class);
        }
        return collected;
    }
    public List<Resource> getSubclasses(String uri) {
        // get the class(es):
        Resource resource = model.getResource(uri);
        ResIterator subclasses = model.listSubjectsWithProperty(RDFS.subClassOf, resource);

        return iteratorToList(subclasses);
    }
    private static <T> List<T> iteratorToList(ExtendedIterator<T> iterator) {
        List<T> list = new ArrayList<>();
        while(iterator.hasNext()) {
            list.add(iterator.next());
        }
        return list;
    }

    /**
     * Returns the short name based on the prefix mapping ns:localname
     * If there is no entry found in the map by either key or value, it will return the full uri
     * @param resource
     * @return
     */
    private String getShortName(Resource resource) {
        if(prefixMap.containsKey(resource.getNameSpace())) {
            return prefixMap.get(resource.getNameSpace()) + ":" + resource.getLocalName();
        }
        if(prefixMap.containsValue(resource.getNameSpace())) {
            for(String key : prefixMap.keySet()) {
                String value = prefixMap.get(key);
                if(value.equals(resource.getNameSpace())) {
                    return key + ":" + resource.getLocalName();
                }
            }
        }
        return resource.getURI();
    }

    public String toGraphString(Resource uri) { return toGraphString(uri, 1); }
    public String toGraphString() { return toGraphString(getTypes().get(0)); }
    public String toGraphString(String uri) { return toGraphString(model.getResource(uri)); }

    /**
     * Generates a graphstring based on the specified resource, it's properties and relationships
     * @param resource
     * @param levels
     * @return
     */
    public String toGraphString(Resource resource, int levels) {
        System.out.println("Loading graph for: " + resource);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder   .append("digraph finite_state_machine {").append(System.lineSeparator())
                        .append("size=\"8,5\"").append(System.lineSeparator())
                        .append("node [shape = circle];").append(System.lineSeparator());

        // add the resource and subclass reference:
        appendResourceSubclasses(stringBuilder, resource, levels);

        stringBuilder.append("}");
        return stringBuilder.toString();

    }
    private void appendResourceSubclasses(StringBuilder stringBuilder, Resource resource, int levels) {
        List<Resource> subClasses = getSubclasses(resource.getURI());
        if(subClasses.isEmpty()) {
            stringBuilder.append(getShortName(resource)).append(System.lineSeparator());
        }
        for(Resource subClass : subClasses) {
            stringBuilder   .append(getShortName(resource))
                            .append(" -> ")
                            .append(getShortName(subClass))
                            .append(" [ label = ").append("\"Subclass\"").append("];")
                            .append(System.lineSeparator())
            ;
            if(levels > 0) {
                appendResourceSubclasses(stringBuilder, subClass, levels - 1);
            }
        }
    }


//    private String getGraphString() {
//        StringBuilder base =
//                "digraph finite_state_machine {\n" +
//                        "\trankdir=LR;\n" +
//                        "\tsize=\"8,5\"\n" +
//                        "\tnode [shape = doublecircle]; LR_0 LR_3 LR_4 LR_8;\n" +
//                        "\tnode [shape = circle];\n" +
//                        "\tLR_0 -> LR_2 [ label = \"SS(B)\" ];\n" +
//                        "\tLR_0 -> LR_1 [ label = \"SS(S)\" ];\n" +
//                        "\tLR_1 -> LR_3 [ label = \"S($end)\" ];\n" +
//                        "\tLR_2 -> LR_6 [ label = \"SS(b)\" ];\n" +
//                        "\tLR_2 -> LR_5 [ label = \"SS(a)\" ];\n" +
//                        "\tLR_2 -> LR_4 [ label = \"S(A)\" ];\n" +
//                        "\tLR_5 -> LR_7 [ label = \"S(b)\" ];\n" +
//                        "\tLR_5 -> LR_5 [ label = \"S(a)\" ];\n" +
//                        "\tLR_6 -> LR_6 [ label = \"S(b)\" ];\n" +
//                        "\tLR_6 -> LR_5 [ label = \"S(a)\" ];\n" +
//                        "\tLR_7 -> LR_8 [ label = \"S(b)\" ];\n" +
//                        "\tLR_7 -> LR_5 [ label = \"S(a)\" ];\n" +
//                        "\tLR_8 -> LR_6 [ label = \"S(b)\" ];\n" +
//                        "\tLR_8 -> LR_5 [ label = \"S(a)\" ];\n" +
//                        "}";
//    }
}
