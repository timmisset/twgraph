package main.graphwindow;

import guru.nidi.graphviz.engine.*;
import org.apache.batik.dom.svg.SAXSVGDocumentFactory;
import org.apache.batik.swing.svg.JSVGComponent;
import org.apache.batik.util.XMLResourceDescriptor;
import org.w3c.dom.svg.SVGDocument;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Graph2SVG {

    static String graphString = "digraph finite_state_machine {\n" +
            "\trankdir=LR;\n" +
            "\tsize=\"8,5\"\n" +
            "\tnode [shape = circle];\n" +
            "\tLR_0 -> LR_2 [ label = \"SS(B)\" ];\n" +
            "\tLR_0 -> LR_1 [ label = \"SS(S)\" ];\n" +
            "\tLR3\n" +
//            "\tLR_1 -> LR_3 [ label = \"S($end)\" ];\n" +
//            "\tLR_2 -> LR_6 [ label = \"SS(b)\" ];\n" +
//            "\tLR_2 -> LR_5 [ label = \"SS(a)\" ];\n" +
//            "\tLR_2 -> LR_4 [ label = \"S(A)\" ];\n" +
//            "\tLR_5 -> LR_7 [ label = \"S(b)\" ];\n" +
//            "\tLR_5 -> LR_5 [ label = \"S(a)\" ];\n" +
//            "\tLR_6 -> LR_6 [ label = \"S(b)\" ];\n" +
//            "\tLR_6 -> LR_5 [ label = \"S(a)\" ];\n" +
//            "\tLR_7 -> LR_8 [ label = \"S(b)\" ];\n" +
//            "\tLR_7 -> LR_5 [ label = \"S(a)\" ];\n" +
//            "\tLR_8 -> LR_6 [ label = \"S(b)\" ];\n" +
//            "\tLR_8 -> LR_5 [ label = \"S(a)\" ];\n" +
            "}";

    // set the parser for the SAXSVGDocumentFactory
    static String parser = XMLResourceDescriptor.getXMLParserClassName();
    static SAXSVGDocumentFactory saxsvgDocumentFactory = new SAXSVGDocumentFactory(parser);

    // use the v8 javascript engine, this doesn't require installation of dot or other dependencies
    static GraphvizV8Engine engine = new GraphvizV8Engine();

    public static SVGDocument toSVG(String graph) throws IOException {
        Graphviz.useEngine(engine);
        String svgData = Graphviz.fromString(graph == null ? graphString : graph).render(Format.SVG).toString();
        InputStream inputStream = new ByteArrayInputStream(svgData.getBytes());
        return saxsvgDocumentFactory.createSVGDocument("http://wwww.myfile.com", inputStream);
    }
    public static JSVGComponent toJSVG(String graphString) throws IOException {
        return toJSVG(toSVG(graphString));
    }
    public static JSVGComponent toJSVG(SVGDocument document) {
        JSVGComponent svgComponent = new JSVGComponent();
        svgComponent.setSVGDocument(document);
        return svgComponent;
    }

}
