package util;

import java.io.InputStream;
import java.net.URL;

public class Util {

    public static URL getSampleFileURL(String filename) {
        return Util.class.getClassLoader().getResource("samples/"+ filename);
    }
    public static InputStream getSampleFile(String filename) {
        return Util.class.getClassLoader().getResourceAsStream("samples/" + filename);
    }

}
