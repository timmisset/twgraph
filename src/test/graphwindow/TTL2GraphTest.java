package test.graphwindow;

import main.graphwindow.TTL2Graph;
import org.junit.Test;
import util.Util;

public class TTL2GraphTest {

    @Test
    public void testGraphExploring() {
        TTL2Graph ttl2Graph = new TTL2Graph(Util.getSampleFile("university.ttl"));
        ttl2Graph.explore("http://xmlns.com/foaf/0.1/Person");
    }

}
