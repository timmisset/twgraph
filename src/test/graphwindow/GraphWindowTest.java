package test.graphwindow;

import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.impl.ToolWindowHeadlessManagerImpl;
import com.intellij.testFramework.JavaProjectTestCase;
import main.graphwindow.GraphWindow;

public class GraphWindowTest extends JavaProjectTestCase {

    @Override
    protected void setUp() throws Exception {
        System.out.println("Running setup");
        super.setUp();
        System.out.println("Finished with setup");
    }

    public void testToolWindowContent() {

        System.out.println("Running test tool window content");

        GraphWindow graphWindow = new GraphWindow();
        ToolWindow window = new ToolWindowHeadlessManagerImpl.MockToolWindow(getProject());
        graphWindow.createToolWindowContent(getProject(), window);

    }

}
